import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { DeleteReqComponent } from 'src/app/delete-req/delete-req.component';
import { ReqpopupComponent } from '../reqpopup/reqpopup.component';

@Component({
  selector: 'app-leavereq',
  templateUrl: './leavereq.component.html',
  styleUrls: ['./leavereq.component.css']
})
export class LeavereqComponent implements OnInit {

  constructor(private http: HttpClient,private dialog: MatDialog, private toastr:ToastrService ) { }
  public n =JSON.parse(localStorage.getItem('data')||'{}');
  public managerDetails:any;
  public data:any;
  public columns = ["Description","From","To","Manager Name","Status","Delete"]

  getDetails(){
    this.http.get("http://172.21.242.104:9999/routes/employee_leavestatus/"+this.n.id).subscribe(res=>{
      // console.log(res)
      if(res){
        this.data = res
        this.data = this.data.msg
        console.log(this.data)
        this.getManager() 
      }
    })
  }
  getManager(){
    var logdata = JSON.parse(localStorage.getItem('data')||'{}');
      this.http.get("http://172.21.242.104:9999/routes/manager_details/"+logdata.id).subscribe(res=>{
        console.log(res)
        this.managerDetails = res;
        this.managerDetails = this.managerDetails.msg;

        // console.log("asdf")
      })
      
  }
  applyLeave(){
    console.log(this.managerDetails)
    var leavereq = this.dialog.open(ReqpopupComponent,{data:this.managerDetails});
    leavereq.afterClosed().subscribe(res=>{
      console.log(res);
      if(res == "true"){
        this.toastr.success("Request sent successfully","Success")
      }
      this.getDetails();
    })
  }
  deleteData(data:any){
    var deleteleave = this.dialog.open(DeleteReqComponent)
    deleteleave.afterClosed().subscribe(res=>{
      if(res == "true"){
        this.http.delete("http://172.21.242.104:9999/routes/delete_leave/"+data.Leave_id).subscribe(res=>{
          console.log(res)
          this.toastr.success("Data deleted successfully","Success")
          this.getDetails();
        })
      }  
    })
   
  }
  ngOnInit(): void {
    this.getDetails()
    
  }
  refresh(){
    this.getDetails();
  }

}
