import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeavereqComponent } from './leavereq/leavereq.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { TicketListComponent } from './ticket-list/ticket-list.component';

const routes: Routes = [
  {path:'leaverequest',component:LeavereqComponent},
  {path:'ticketlist',component:TicketListComponent},
  {path:'projectlist',component:ProjectListComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
