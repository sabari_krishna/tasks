import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ChangeStatusComponent } from '../change-status/change-status.component';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css']
})
export class TicketListComponent implements OnInit {

  constructor(private http:HttpClient,private dialog: MatDialog, private toastr: ToastrService ) { }
  public data:any;
  public columns = ["Ticket Id","Project Id","Manager Id","Description","Start Date","End Date","Status","Action"]
  ngOnInit(): void {
    this.getTicket()

  }
  getTicket(){
    var logdata = JSON.parse(localStorage.getItem('data')||'{}');
    console.log(logdata)
      this.http.get("http://172.21.242.104:9999/routes/get_details/"+logdata.id).subscribe(res=>{
        console.log(res)
        this.data = res
        this.data = this.data.msg
        console.log(this.data.managerId)
      })
  }
  editTicket(data:any){
    // console.log(data)
    var changestatus = this.dialog.open(ChangeStatusComponent,{data:data.ticket_id})
    changestatus.afterClosed().subscribe(res=>{
      if(res == "true"){
        this.toastr.success("Status Updated Successfully","Success")
      }
      this.getTicket()
    })
  }

}
