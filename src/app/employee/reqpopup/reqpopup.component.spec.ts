import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqpopupComponent } from './reqpopup.component';

describe('ReqpopupComponent', () => {
  let component: ReqpopupComponent;
  let fixture: ComponentFixture<ReqpopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReqpopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
