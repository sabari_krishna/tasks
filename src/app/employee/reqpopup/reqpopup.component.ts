import { HttpClient } from '@angular/common/http';
import { Component, Inject, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { th } from 'date-fns/locale';

@Component({
  selector: 'app-reqpopup',
  templateUrl: './reqpopup.component.html',
  styleUrls: ['./reqpopup.component.css']
})
export class ReqpopupComponent implements OnInit {

  constructor(private http:HttpClient,@Inject(FormBuilder) public formBuilder: FormBuilder,@Inject(MAT_DIALOG_DATA) 
  public data:Object,private dialogRef:MatDialogRef<ReqpopupComponent>, private ngZone: NgZone) { 
    this.formdata = data;
  }
  public logdata = JSON.parse(localStorage.getItem('data')||'{}');  
  public empId:any;
  public managerId:any;
  public managerName:any;
  public minDate = new Date();
  public leaveForm!:FormGroup;
  public formdata:any;
  ngOnInit(): void {
    console.log(this.formdata)
    console.log("test")
    // console.log(this.formdata)
    // console.log(this.logdata)
    this.empId = this.logdata.id
    this.managerId = this.formdata.manager_id
    this.managerName = this.formdata.manager_name
    this.leaveForm = this.formBuilder.group({
      employee_id: new FormControl(this.logdata.id,[Validators.required]),
      employee_name:new FormControl(this.logdata.name,[Validators.required]),
      Manager_id: new FormControl(this.managerId,[Validators.required]),
      From_date: new FormControl('',[Validators.required]),
      To_date: new FormControl('',[Validators.required]),
      Description: new FormControl('',[Validators.required]),
      manager_name:new FormControl(this.managerName,[Validators.required])
    })
    console.log(this.leaveForm)
  }
  
  close(){
    this.ngZone.run(()=>{
      this.dialogRef.close("false");
    })

  }
  leaveData(){
      this.http.post("http://172.21.242.104:9999/routes/leave_request/",this.leaveForm.value).subscribe(res=>{
        console.log(res);
        this.ngZone.run(()=>{
          this.dialogRef.close("true");
        })
      })
  }

}
