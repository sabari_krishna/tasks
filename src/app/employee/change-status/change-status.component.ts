import { HttpClient } from '@angular/common/http';
import { Component, Inject, NgZone, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-change-status',
  templateUrl: './change-status.component.html',
  styleUrls: ['./change-status.component.css']
})
export class ChangeStatusComponent implements OnInit {
  public ticketData:any;
  constructor(private http: HttpClient,@Inject(MAT_DIALOG_DATA) public data:object,private dialogRef:MatDialogRef <ChangeStatusComponent>, private ngZone:NgZone) {
      this.ticketData = data
   }

  ngOnInit(): void {
  }
  status = new FormGroup({
    changestatus:new FormControl('',[Validators.required])
  })
  close(){
    this.ngZone.run(()=>{
      this.dialogRef.close("false");
    })

  }
  edit(){
      this.http.put("http://172.21.242.104:9999/routes/update_ticket_employee/"+this.ticketData,this.status.value).subscribe(res=>{
        console.log(res)
        this.ngZone.run(()=>{
          this.dialogRef.close("true");
        })
      })
  }


}
