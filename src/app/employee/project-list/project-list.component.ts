import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ViewProjectComponent } from 'src/app/manager/view-project/view-project.component';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  constructor(private dialog:MatDialog,private http:HttpClient) { }
  data:any;
  public columns:any[] = ["Project_Id","Project_Name","Manager Id","Manager_Name","Status"]

  ngOnInit(): void {
    this.getProjects()
  }
  clickedRows(data:any){

    this.dialog.open(ViewProjectComponent,{data:data})
  }
  getProjects(){
    var n =JSON.parse(localStorage.getItem('data')||'{}');
    console.log(n)
    this.http.get("http://172.21.242.104:9999/routes/project_assigned/"+n.id).subscribe(res=>{
      console.log(res)
      this.data = res;
      this.data = this.data.msg
    })
   }

}
