import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { LeavereqComponent } from './leavereq/leavereq.component';
import { AngularMaterialModule } from '../angular-material.module';
import { ReqpopupComponent } from './reqpopup/reqpopup.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { ChangeStatusComponent } from './change-status/change-status.component';
import { ProjectListComponent } from './project-list/project-list.component';


@NgModule({
  declarations: [
    SidenavComponent,
    LeavereqComponent,
    ReqpopupComponent,
    TicketListComponent,
    ChangeStatusComponent,
    ProjectListComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    MDBBootstrapModule.forRoot(),
    NgbCollapseModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
    
  ]
})
export class EmployeeModule { }
