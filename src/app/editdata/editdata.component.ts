import { HttpClient } from '@angular/common/http';
import { AfterViewInit,Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AdduserComponent } from '../adduser/adduser.component';
import { DeleteReqComponent } from '../delete-req/delete-req.component';
import { EditpopupComponent } from '../editpopup/editpopup.component';
import { ToastrService } from 'ngx-toastr';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';


@Component({
  selector: 'app-editdata',
  templateUrl: './editdata.component.html',
  styleUrls: ['./editdata.component.css']
})

export class EditdataComponent implements OnInit {
  public data:any;
  
  public count=0;
  
  constructor(private http: HttpClient, private dialog: MatDialog, private toastr:ToastrService, private router :Router) { }
  public columns:any[] = ["Id","Name","Mobile","Designation","Email","Action"]
  public response:any;

  ngOnInit(): void {
    this.refreshData();
    }
    logout(){
      localStorage.removeItem('data')
      this.router.navigate(['/login'])
    }
  addUser(){
    var adduser = this.dialog.open(AdduserComponent);
    adduser.afterClosed().subscribe(res=>{
      console.log(res);
      if(res == "Successfully Stored in Database"){
        this.refreshData();
        this.toastr.success("User created successfully","success")
      }
      if(res == "User already exists"){
        this.toastr.error("User Already Exist","Error")
      }
       
    })
  }
  editData(data:any){
    console.log(data)
    var dialogRef = this.dialog.open(EditpopupComponent,{data:data})
    dialogRef.afterClosed().subscribe(res=>{
      console.log(res)
      if(res == "Success"){
        this.refreshData();
        this.toastr.success("Data updated successfully","success")
      }

    })
  }
  refreshData(){
    console.log("re")
    this.http.get("http://172.21.242.104:9999/routes/all").subscribe((res)=>{
      
    if(res){        
      this.data = res
        this.data = this.data.msg
        console.log("data updated")
        console.log(this.data )
      }
  

    })
  }

  
deleteData(data:any){

    var delete_req = this.dialog.open(DeleteReqComponent,{data:data.Name})
    delete_req.afterClosed().subscribe(res=>{
      if(res == "true"){
        this.http.delete("http://172.21.242.104:9999/routes/delete_user/"+ data.Id).subscribe((res)=>{
          this.response = res
          this.refreshData()
          if(this.response.msg == "Success"){
            this.toastr.success("Data Deleted successfully","success")
          }
          else{
            this.toastr.error("Can't be deleted","error")
          }
          // console.log(res)
        //   this.http.get("http://172.21.242.104:9999/routes/all").subscribe(res=>{
        //   this.data=res
        // })
      })
      }
    })
  // })
}

}
