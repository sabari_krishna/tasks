import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditdataComponent } from './editdata/editdata.component';
import { LoginComponent } from './login/login.component';




const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path:'edit',component:EditdataComponent},
  {path:'manager',loadChildren:()=> import('./manager/manager.module').then(m=>m.ManagerModule)},
  {path:'employee',loadChildren:()=>import('./employee/employee.module').then(e=>e.EmployeeModule)},
  
  
  // {path:'dashboard',component:DashboardComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
