import { Component, NgZone, OnInit } from '@angular/core';
import { validateBasis } from '@angular/flex-layout';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef } from '@angular/material/dialog'; 
@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {
  response:any

  constructor(private http: HttpClient,private toastr:ToastrService,private dialogRef: MatDialogRef<AdduserComponent>,private ngZone: NgZone) { 
    dialogRef.disableClose = true
  }
  create = new FormGroup({
    Id:new FormControl('',[Validators.required]),
    Name:new FormControl('',[Validators.required]),
    Mob_num:new FormControl('',[Validators.required,Validators.minLength(10),Validators.maxLength(10),Validators.min(6000000000),Validators.max(9999999999),Validators.pattern('[0-9]*')]),
    Email:new FormControl('',[Validators.required,Validators.email]),
    Password: new FormControl('',[Validators.required]),
    Designation: new FormControl('',[Validators.required]),
    // profile: new FormControl('',[Validators.required])
  })
  addData(){
    this.createData()
  }
  get Mobile(){
    return this.create.get('Mob_num')
  }
  createData(){
    this.http.post("http://172.21.242.104:9999/routes/create_user",this.create.value).subscribe(res=>{
      console.log(res)
      this.response = res;
      this.ngZone.run(()=>{
        this.dialogRef.close(this.response.msg);
      })
    })
  }
  

  ngOnInit(): void {
  }

}
