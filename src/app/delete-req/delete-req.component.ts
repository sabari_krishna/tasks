import { Component, OnInit,Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-delete-req',
  templateUrl: './delete-req.component.html',
  styleUrls: ['./delete-req.component.css']
})
export class DeleteReqComponent implements OnInit {

  public deletedata:any;

  constructor(@Inject(MAT_DIALOG_DATA) 
  public data:Object) {
      this.deletedata = data
      
   }
  

  ngOnInit(): void {
  }

}
