import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../user.service';
import { th } from 'date-fns/locale';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private http: HttpClient, private router:Router,private toastr:ToastrService,private userservice: UserService) { }
  res:any
  loginDetails:object|undefined;
  n:any|undefined;

  ngOnInit(): void {
  }
  loginForm = new FormGroup({
    user_name: new FormControl('',[Validators.required]),
    password: new FormControl('',[Validators.required])

  })
  login(){
    localStorage.removeItem('data');
      this.http.post("http://172.21.242.104:9999/routes/login",this.loginForm.value).subscribe((response:object)=>{
        console.log(response)
        this.res = response;
       this.loginDetails = {
         name:this.res.Name,
         id:this.res.Id
       }
       localStorage.setItem('data',JSON.stringify(this.loginDetails));
       this.n = (localStorage.getItem('data'))
       console.log(this.n)
        if(this.res.role == "Admin"){
          this.router.navigate(['/edit']);
          this.toastr.success("logged In","success");
        }
        else if (this.res.role == "Manager"){
            this.router.navigate(['/home'])
        }
        else if(this.res.role == "Employee"){
          this.router.navigate(['/projectlist'])
        }
        else if(this.res.msg == "Invalid login"){
          this.toastr.error("UserName or Password wrong","Error");
        }
        else if(this.res.msg == "Cannot find user"){
          this.toastr.error("User not Found","Error")
        }
      })

 }
}
