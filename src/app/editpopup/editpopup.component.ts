import { Component, Inject, Injectable, NgZone, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
// import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-editpopup',
  templateUrl: './editpopup.component.html',
  styleUrls: ['./editpopup.component.css']
})
export class EditpopupComponent implements OnInit {
public formdata:any;
public Id:any;
  update!: FormGroup;
public Name:any;
public Mob_num:any;
public Password:any;
public Designation:any;
public response:any;
public email:any;
  constructor(@Inject(MAT_DIALOG_DATA) 
  public data:Object,@Inject(FormBuilder) public formBuilder: FormBuilder,private http: HttpClient,private toastr:ToastrService, private dialogRef: MatDialogRef<EditpopupComponent>,private ngZone: NgZone) {
    this.formdata = data
  }
  

  updateData(){
    console.log(this.update.value)
    
    // console.log(this.update.value);
    this.http.put("http://172.21.242.104:9999/routes/update_user/",this.update.value).subscribe((res)=>{
      console.log(res)
      this.response = res
      if(res){
        this.ngZone.run(()=>{
          this.dialogRef.close(this.response.msg);
        })
      }
    })
  }

  
  ngOnInit(): void {
    
    console.log(this.formdata)
    this.Id = this.formdata.Id
    this.Name = this.formdata.Name
    this.Password = this.formdata.Password
    this.Mob_num = this.formdata.Mob_num
    this.Designation = this.formdata.Designation
    this.email = this.formdata.Email
    // this.Iu = this.formdata.Id
    this.update = this.formBuilder.group({
      Id:new FormControl(this.Id,Validators.required),
      Name: new FormControl(this.Name,Validators.required ),
      Password:new FormControl(this.Password,Validators.required),
      Mob_num:new FormControl(this.Mob_num,Validators.required),
      Designation:new FormControl(this.Designation,Validators.required)   ,
      email:new FormControl (this.email,[Validators.required])
    })

 
  }
 
  // close() {
  //   this.activeModal.close({ dataModified: false });
  // }
  

}
