import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaComponent } from './widgets/area/area.component';
import { CardComponent } from './widgets/card/card.component';
import { PieComponent } from './widgets/pie/pie.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [
    AreaComponent,
    CardComponent,
    PieComponent
  ],
  imports: [
    CommonModule,
    HighchartsChartModule,
    MatIconModule,
    FlexLayoutModule
  ],

  exports :[
    AreaComponent,
    CardComponent,
    PieComponent
  ]
})
export class SharedModule { }
