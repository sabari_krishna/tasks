import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagerRoutingModule } from './manager-routing.module';
import { AngularMaterialModule } from '../angular-material.module';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
// import { T1Component } from './t1/t1.component';
import { IgxCalendarModule,IgxDialogModule } from 'igniteui-angular';
import { HomeComponent } from './home/home.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { ProjectComponent } from './project/project.component';
import { CreateProjectComponent } from './create-project/create-project.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectSearchModule } from 'mat-select-search';
import { RequestComponent } from './request/request.component';
import { ViewProjectComponent } from './view-project/view-project.component';
import { TicketComponent } from './ticket/ticket.component';
import { CreateTicketComponent } from './create-ticket/create-ticket.component';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import {  MDBBootstrapModule } from 'angular-bootstrap-md';
import { EditReqComponent } from './edit-req/edit-req.component';
import { LinechartComponent } from './linechart/linechart.component';
import { ChartsModule } from 'ng2-charts';

import { SharedModule } from '../shared/shared.module';








@NgModule({
  declarations: [
    DashboardComponent,
    HomeComponent,
    ProjectComponent,
    CreateProjectComponent,
    RequestComponent,
    ViewProjectComponent,
    TicketComponent,
    CreateTicketComponent,
    EditReqComponent,
    LinechartComponent,
    // T1Component
  ],
  imports: [
    CommonModule,
    ManagerRoutingModule,
    AngularMaterialModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    AngularMaterialModule,
    IgxCalendarModule,
    IgxDialogModule,
    FullCalendarModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatSelectSearchModule,
    NgbCollapseModule,
    MDBBootstrapModule.forRoot(),
    ChartsModule,
    SharedModule
  
  ]
})
export class ManagerModule { }
