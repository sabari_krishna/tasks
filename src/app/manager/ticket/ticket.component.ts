import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { DeleteReqComponent } from 'src/app/delete-req/delete-req.component';
import { CreateTicketComponent } from '../create-ticket/create-ticket.component';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {
  public data:any;
  public logdata = JSON.parse(localStorage.getItem('data')||'{}');
  constructor(private dialog: MatDialog,private http: HttpClient, private toastr:ToastrService) { }
  public columns:any[] = ["Project Id","Ticket Id","Description","Start Date","End Date","Status","Close","Action"]

  getData(){
    this.http.get("http://172.21.242.104:9999/routes/get_all_ticket/"+this.logdata.id).subscribe(res=>{
      this.data =res
      this.data = this.data.msg
      console.log(res)
      console.log(this.data)
    })
  }
  ngOnInit(): void {
    this.getData()
  }
  deleteTicket(data:any){
    var deleteReq = this.dialog.open(DeleteReqComponent);
    deleteReq.afterClosed().subscribe(res=>{
      if(res == "true"){
        this.http.delete("http://172.21.242.104:9999/routes/Delete_ticket/"+data.ticket_id).subscribe(res=>{
      console.log(res)
      this.getData();
    })
      this.toastr.success("Ticket deleted successfully","success");
      
      }
    })
    
  }
  closeTicket(data:any){
    var closeReq = this.dialog.open(DeleteReqComponent);
    closeReq.afterClosed().subscribe(res=>{
      if(res == "true"){
        this.http.put("http://172.21.242.104:9999/routes/close_ticket/"+data.ticket_id,"").subscribe(res=>{
          if(res){
            console.log(res)
            this.getData();

          }
      
    })
    this.toastr.success("Ticket closed successfully","success");
    
      }
    })

  }
  createTicket(){
    var response;
    var create = this.dialog.open(CreateTicketComponent);
    create.afterClosed().subscribe( res=>{
      console.log(res)
    
          if(res == "true"){
            this.getData();
          this.toastr.success("Ticket created successfully","Success")
        }
        
      
      // if(res =="true"){
      //   console.log(res)
      //   this.getData();
      //   this.toastr.success("Ticket created successfully","success")
      // }
    })
    
  }
  
}

