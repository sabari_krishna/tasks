import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { CreateProjectComponent } from '../create-project/create-project.component';
import { HttpClient } from '@angular/common/http';
import { ViewProjectComponent } from '../view-project/view-project.component';
import { DeleteReqComponent } from 'src/app/delete-req/delete-req.component';
import { EditReqComponent } from '../edit-req/edit-req.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  constructor(private dialog: MatDialog, private http: HttpClient,private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getProjects();
  }
  data:any;
  public columns:any[] = ["Project_Id","Project_Name","Manager_Name","Status","Action"]
 create(){
   var create = this.dialog.open(CreateProjectComponent);
   create.afterClosed().subscribe(res=>{
     if(res == "true"){
       this.toastr.success("Project Created Successfully","Success")
       this.getProjects();
     }
   })

 }
 deleteData(data:any){
  //  console.log(data)
  var delete_req = this.dialog.open(DeleteReqComponent,{data:data.Project_Name});
  delete_req.afterClosed().subscribe(res=>{
    if(res == "true"){
      this.http.delete("http://172.21.242.104:9999/routes/delete/"+data.project_id).subscribe(res=>{
     console.log(res)
     if(res){
       this.toastr.success("Project Deleted Successfully","Success");
       this.getProjects();
     }
   }) 
    }
  })
   

 }
 clickedRows(data:any){

   this.dialog.open(ViewProjectComponent,{data:data})
 }
 editData(data:any){
   var edit = this.dialog.open(EditReqComponent,{data:data})
   edit.afterClosed().subscribe(res=>{
     if(res == "true"){
       this.getProjects();
       this.toastr.success("Project Updated Successfully","Success")
     }
   })
 }
 
 getProjects(){
  var n =JSON.parse(localStorage.getItem('data')||'{}');
  console.log(n)
    this .http.get("http://172.21.242.104:9999/routes/project_details/"+n.id).subscribe(res=>{
      console.log(res)
      this.data = res
      this.data = this.data.msg
    })
 }
}
