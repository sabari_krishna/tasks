import { Component, NgZone, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ChangeDetectionStrategy } from '@angular/compiler';
import { HttpClient } from '@angular/common/http';
import { UserService } from 'src/app/user.service';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit {

  constructor(private http: HttpClient, private dialogRef: MatDialogRef<CreateProjectComponent>,private ngZone:NgZone) { }
  filteredData: Record<string, string>[] = []
  emp:any
  response:any;
  n:any|undefined
  name:string|undefined
  id:any|undefined
  create!: FormGroup;

  ngOnInit(): void {
    this.getEmp()
    this.n =JSON.parse(localStorage.getItem('data')||'{}');
    this.name = this.n.name
    this.id = this.n.id
    this.create = new FormGroup({
      proName: new FormControl('',[Validators.required]),
      managerName: new FormControl(this.name,[Validators.required]),
      managerId: new FormControl(this.id,[Validators.required]),
      team: new FormControl('',[Validators.required]),
      Status:new FormControl('on going',[Validators.required])
    })
  }
  getEmp(){
    // console.log(this.userservice.userId)
    this.http.get("http://172.21.242.104:9999/routes/get_by_emp/").subscribe(res=>{
        this.emp = res
        this.emp = this.emp.msg
        // console.log(this.emp)
    })
  }

  
  createProject(){

    this.http.post("http://172.21.242.104:9999/routes/create_project/",this.create.value).subscribe(res=>{
      console.log(res);
      this.response = res
      if(this.response.code == 200){
        this.ngZone.run(()=>{
          this.dialogRef.close("true");
        })
      }
    })
  }

}
