import { Component, OnInit,Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-view-project',
  templateUrl: './view-project.component.html',
  styleUrls: ['./view-project.component.css']
})
export class ViewProjectComponent implements OnInit {
  formdata: any;
  team:any;

  constructor(@Inject(MAT_DIALOG_DATA) 
  public data:Object, private router: Router) { 
    this.formdata = data
    this.team = this.formdata.team
  }
  navigateToTicket(){
    this.router.navigate(['/tickets'])
  }

  ngOnInit(): void {
    console.log(this.formdata)
  }
  

}
