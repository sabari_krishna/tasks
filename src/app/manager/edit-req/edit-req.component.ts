import { HttpClient } from '@angular/common/http';
import { Component, Inject, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-req',
  templateUrl: './edit-req.component.html',
  styleUrls: ['./edit-req.component.css']
})
export class EditReqComponent implements OnInit {


  public emp:any;
  public formData:any;
  public data:any;
  public projectName:any;
  public update!:FormGroup;
  // public update!:FormGroup;
  constructor(private http: HttpClient,@Inject(FormBuilder) public formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public matdata:object, private dialogRef: MatDialogRef<EditReqComponent>, private ngZone: NgZone, private toastr: ToastrService ) {
    this.formData = matdata;
   }
  updateData(){
    this.http.put("http://172.21.242.104:9999/routes/update_project/"+this.formData.project_id,this.update.value).subscribe(res=>{
      console.log(res)
        this.emp = res;
        if(this.emp.code == 200){
          this.ngZone.run(()=>{
            this.dialogRef.close("true");
          })
        }
        this.emp = this.emp.msg;
    })
  }


  ngOnInit(): void {
    this.getEmp()
    console.log(this.formData)
    this.update = this.formBuilder.group({
      proName: new FormControl(this.formData.project_name,[Validators.required]),
      team: new FormControl('',[Validators.required])
    })
  }
  getTeam(){
    console.log(this.emp.team)
  }
  getEmp(){
    // console.log(this.userservice.userId)
    this.http.get("http://172.21.242.104:9999/routes/get_by_emp/").subscribe(res=>{
      console.log(res)
        this.emp = res;
        this.emp = this.emp.msg;
        // console.log(this.emp)
    })
  }


}
