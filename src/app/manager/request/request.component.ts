import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

  constructor(private http: HttpClient, private toastr: ToastrService) { }
  public n =JSON.parse(localStorage.getItem('data')||'{}');
  data:any;
  leaveData:any;
  Status:any;
  public columns:any[] = ["Leave Id","Employee Id","Employee Name","Description","From Date","To Date","Status","Action"]
  ngOnInit(): void {
    this.getLeaveData()
  }

  changeValue(value:any,data:any){
      if(value == "accept"){
        console.log("1")
        this.Status = "Approved"

        this.http.put("http://172.21.242.104:9999/routes/leave_update/"+data.Leave_id,{Status:this.Status}).subscribe(res=>{
          console.log(res)
          if(res){
            this.getLeaveData()
            this.toastr.success("Data Updated Successfilly","Success");
          }
          
      })
    }
      else if(value == "decline"){
        this.Status = "Rejected"
        this.http.put("http://172.21.242.104:9999/routes/leave_update/"+data.Leave_id,{Status:this.Status}).subscribe(res=>{
          if(res){
            this.getLeaveData()
            this.toastr.success("Data Updated Successfilly","Success");
          }
      })
      }
  }
  getLeaveData(){
      this.http.get("http://172.21.242.104:9999/routes/display_status/"+this.n.id).subscribe(res=>{
        console.log(res);
        if(res){
          this.leaveData = res;
          this.data  = this.leaveData.msg;
        }
       
      })
  }
}
