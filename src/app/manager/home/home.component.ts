import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  selected: Date | undefined;
  bigChart : any = [] ;
  cards : any = [];
  pieChart : any  = [];

  // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  // dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  // @ViewChild(MatPaginator, { static: true })
  // paginator!: MatPaginator;

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.bigChart = this.homeService.bigChart();
    this.cards = this.homeService.cards();
    this.pieChart = this.homeService.pieChart();

    // this.dataSource.paginator = this.paginator;
  }

}
