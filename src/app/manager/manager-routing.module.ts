import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { ProjectComponent } from './project/project.component';
import { RequestComponent } from './request/request.component';
import { TicketComponent } from './ticket/ticket.component';


const routes: Routes = [
  {path:'dashboard',component:DashboardComponent},
  {path:'home',component:HomeComponent},
  {path:'project',component:ProjectComponent},
  {path:'request',component:RequestComponent},
  {path:'tickets',component:TicketComponent}
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerRoutingModule { }
