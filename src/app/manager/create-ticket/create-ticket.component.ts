import { HttpClient } from '@angular/common/http';
import { Component, NgZone, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatRadioButton } from '@angular/material/radio';
import { da } from 'date-fns/locale';


@Component({
  selector: 'app-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.css']
})
export class CreateTicketComponent implements OnInit {
  public logData =JSON.parse(localStorage.getItem('data')||'{}');

  constructor(private http: HttpClient, private dialogRef: MatDialogRef<CreateTicketComponent>,private ngZone:NgZone) { }
  public data:any;
  public employee: any;
  minDate = new Date();
  public response:any
  ngOnInit(): void {
    this.getProjects()
    console.log(this.logData)
  }
  create = new FormGroup({
    managerId: new FormControl(this.logData.id,[Validators.required]),
    projectId: new FormControl('',[Validators.required]),
    employee_id: new FormControl('',[Validators.required]),
    ticket_description:new FormControl('',[Validators.required]),
    start_date:new FormControl('',[Validators.required]),
    end_date:new FormControl('',[Validators.required])
  })
  createTicket(){
      this.http.post("http://172.21.242.104:9999/routes/create_ticket/",this.create.value).subscribe(res=>{
        console.log(res)
        this.response = res
        if(this.response.code == 200){
          this.ngZone.run(()=>{
            this.dialogRef.close("true");
          })
        }
      })
  }
  getProjects(){
    var n =JSON.parse(localStorage.getItem('data')||'{}');
    console.log(n)
      this.http.get("http://172.21.242.104:9999/routes/project_details/"+n.id).subscribe(res=>{
        console.log(res)
        this.data = res
        this.data = this.data.msg
      })
   }
   getEmployee(data:any){
    console.log(this.create.value)
    //  this.pId = data
     console.log(data)
     this.http.get("http://172.21.242.104:9999/routes/employee_details/"+data).subscribe(res=>{
       console.log(res)
       this.employee = res
       this.employee = this.employee.msg;


     })
     
   }

}
